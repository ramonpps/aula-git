def helenia(string):
    if string == 'loid':
        k = 'paladino'
    elif string == 'cloch':
        k = 'druida'
    elif string == 'theo':
        k = 'feiticeiro'
    elif string == 'hanali':
        k = 'ladino'
    elif string == 'skadi':
        k = 'patrulheiro'
    else:
        k = 'nao existe'
    return k

k = input('Qual é o nome do personagem?\n')
print('A classe do personagem', k, 'é:', helenia(k))

